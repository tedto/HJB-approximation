# Hamilton-Jacobi-Bellman approximation

Rewrite of Benjamin Moll's [HJB `MATLAB`](https://benjaminmoll.com/codes/) code using Python. The code is provided as `jupyter` notebooks.

At this stage, I have only rewritten:

 * [HJB_NGM.m](http://benjaminmoll.com/wp-content/uploads/2020/06/HJB_NGM.m)
 * [HJB_NGM_skiba.m](http://benjaminmoll.com/wp-content/uploads/2020/06/HJB_NGM_skiba.m)

No effort has been made at improving the original code.

**Note:** João Duarte has done a much more complete job of translating Benjamin Moll's [code to Python](https://github.com/jbduarte/Numerical_Continuous_Time).
